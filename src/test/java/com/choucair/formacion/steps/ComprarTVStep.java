package com.choucair.formacion.steps;

import org.fluentlenium.core.annotation.Page;

import com.choucair.formacion.pageobjects.ComprarTVPage;

import net.thucydides.core.annotations.Step;

public class ComprarTVStep {

	@Page
	ComprarTVPage comprarTVPage;

	@Step
	public void AbroNavegador() {
		comprarTVPage.open();
	}

	@Step
	public void BuscoPalabra(String tv) throws InterruptedException {
		comprarTVPage.BuscoPalabra(tv);
	}

	@Step
	public void ImprimoInfomacionTVs() {
		comprarTVPage.ImprimoInfomacionTVs();
	}

	@Step
	public void SeleccionoTV() {
		comprarTVPage.SeleccionoTV();
	}

	@Step
	public void Comprar() throws InterruptedException {
		comprarTVPage.Comprar();
	}

	@Step
	public void VerificoProductoAgregado() {
		comprarTVPage.VerificoProductoAgregado();
	}

}
