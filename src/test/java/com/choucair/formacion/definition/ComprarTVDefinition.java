package com.choucair.formacion.definition;

import com.choucair.formacion.steps.ComprarTVStep;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ComprarTVDefinition {

	@Steps
	ComprarTVStep comprarTVStep;

	@Given("^que quiero comprar un televisor para ver el mundial de futbol$")
	public void que_quiero_comprar_un_televisor_para_ver_el_mundial_de_futbol() throws Throwable {
		comprarTVStep.AbroNavegador();
	}

	@When("^ingreso a la página virtual del éxito y selecciono el \"([^\"]*)\" que más me gusta$")
	public void ingreso_a_la_página_virtual_del_éxito_y_selecciono_el_que_más_me_gusta(String tv) throws Throwable {
		comprarTVStep.BuscoPalabra(tv);
		comprarTVStep.ImprimoInfomacionTVs();
		comprarTVStep.SeleccionoTV();

	}

	@Then("^realizo la compra para que sea enviado a mi casa\\.$")
	public void realizo_la_compra_para_que_sea_enviado_a_mi_casa() throws Throwable {

		comprarTVStep.Comprar();
		comprarTVStep.VerificoProductoAgregado();
	}
}
