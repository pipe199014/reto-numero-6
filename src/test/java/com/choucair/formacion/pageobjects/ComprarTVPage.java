package com.choucair.formacion.pageobjects;

import org.hamcrest.MatcherAssert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.exito.com/")
public class ComprarTVPage extends PageObject {

	private static WebElement primerTV;

	// LabelBusqueda
	@FindBy(id = "tbxSearch")
	private WebElementFacade txtBuscar;

	// Botón busqueda
	@FindBy(id = "btnSearch")
	private WebElementFacade btnBuscar;

	// Link pulgadas del TV
	@FindBy(xpath = "//*[@id ='filterBy']//*[@class ='mobil-link'][contains(text(),'de 44 a 49')]")
	private WebElementFacade lnkPulgadasTv;

	// Check marca del TV
	@FindBy(xpath = "//*[@id ='filterBy']//*[contains(text(),'LG')]/input")
	private WebElementFacade chkMarcaTV;

	// Link resolución
	@FindBy(xpath = "//*[@id ='filterBy']//*[text() = 'UHD-4K ']")
	private WebElementFacade lnkResolucion;

	// Tabla de productos
	@FindBy(xpath = "//*[@class = 'row product-list']")
	private java.util.List<WebElement> TblProductos;

	// Boton añadir carrito
	@FindBy(xpath = "//*[contains(@class, 'form-group col-addtocart')]/button")
	private WebElementFacade btnAgregarCarrito;

	// Icono carrito de compras
	@FindBy(xpath = "//*[contains(@class, 'header-shopping-cart')]/a/span")
	private WebElementFacade icnCarrito;

	// Cerrar Popup
	@FindBy(xpath = "//*[contains(@id,'bunting_lightbox')]/a")
	private WebElementFacade cerrarPopup;

	// label productos agregados
	@FindBy(xpath = "//*[contains(@id, 'summary-page')]//*[contains(text(), 'en tu carrito')]")
	private WebElementFacade lblProductosAgregados;

	public void BuscoPalabra(String tv) throws InterruptedException {
		txtBuscar.sendKeys(tv);
		btnBuscar.click();
		lnkPulgadasTv.click();
		chkMarcaTV.click();
		JavascriptExecutor j = (JavascriptExecutor) this.getDriver();
		j.executeScript("window.scrollBy(0,200)", "");
		lnkResolucion.click();
		Thread.sleep(3000);
	}

	public void ImprimoInfomacionTVs() {
		int cont = 0;

		for (WebElement trElement : TblProductos) {
			java.util.List<WebElement> tblColumna = trElement
					.findElements(By.xpath("div//*[contains(@class, 'row')]/a"));

			System.out.println(trElement.getText());

			for (WebElement tdElement2 : tblColumna) {
				if (cont == 0) {
					primerTV = tdElement2;
				}
				cont++;
			}

		}

	}

	public void SeleccionoTV() {
		primerTV.click();
	}

	public void Comprar() throws InterruptedException {
		btnAgregarCarrito.click();
		Thread.sleep(4000);

		if (cerrarPopup.isCurrentlyVisible())
			cerrarPopup.click();

		Thread.sleep(3000);
		icnCarrito.click();
		Thread.sleep(3000);
	}

	public void VerificoProductoAgregado() {

		MatcherAssert.assertThat("No se encontraron productos", lblProductosAgregados.isDisplayed());
	}

}
